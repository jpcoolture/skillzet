<?php
/**
 * Default header template
 *
 * @package base-html
 */

$header = rella_get_header_layout();
?>
<header <?php rella_helper()->attr( 'header', $header['attributes'] ); ?>>

	<?php 
		
		$header_content = get_post_field( 'post_content', $header['id'] );

		echo do_shortcode( $header_content );
		
	?>

</header>