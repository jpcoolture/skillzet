/*!
* Themerella
*
* (c) Copyright themerella.com
*
* @version 1.0.0
* @author  Themerella
*/



(function($) {
    'use strict';
    var instanceName = '__smoothMouseWheel';
    var SmoothMouseWheel = function(el, options) {
        return this.init(el, options);
    };
    SmoothMouseWheel.defaults = {};
    SmoothMouseWheel.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, SmoothMouseWheel.defaults, options);
            return this;
        },
        build: function() {
            var self = this, container, running = false, currentY = 0, targetY = 0, oldY = 0, maxScrollTop = 0, minScrollTop, direction, onRenderCallback = null, fricton = .92, vy = 0, stepAmt = 2, minMovement = .12, ts = .11;
            var updateScrollTarget = function(amt) {
                targetY += amt;
                vy += (targetY - oldY) * stepAmt;
                oldY = targetY;
            };
            var render = function() {
                if (vy < -minMovement || vy > minMovement) {
                    currentY = currentY + vy;
                    if (currentY > maxScrollTop) {
                        currentY = vy = 0;
                    } else if (currentY < minScrollTop) {
                        vy = 0;
                        currentY = minScrollTop;
                    }
                    container.scrollTop(-currentY);
                    vy *= fricton;
                    if (onRenderCallback) {
                        onRenderCallback();
                    }
                }
            };
            var animateLoop = function() {
                if (!running) return;
                requestAnimFrame(animateLoop);
                render();
            };
            var onWheel = function(e) {
                e.preventDefault();
                var evt = e.originalEvent;
                var delta = evt.detail ? evt.detail * -1 : evt.wheelDelta / 40;
                var dir = delta < 0 ? -1 : 1;
                if (dir != direction) {
                    vy = 0;
                    direction = dir;
                }
                currentY = -container.scrollTop();
                updateScrollTarget(delta);
            };
            window.requestAnimFrame = function() {
                return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(callback) {
                    window.setTimeout(callback, 1e3 / 60);
                };
            }();
            var normalizeWheelDelta = function() {
                'use strict';
                var PIXEL_STEP = 25;
                var LINE_HEIGHT = 50;
                var PAGE_HEIGHT = document.innerHeight;
                function normalizeWheel(event) {
                    var sX = 0, sY = 0, pX = 0, pY = 0;
                    if ('detail' in event) {
                        sY = event.detail;
                    }
                    if ('wheelDelta' in event) {
                        sY = -event.wheelDelta / 120;
                    }
                    if ('wheelDeltaY' in event) {
                        sY = -event.wheelDeltaY / 120;
                    }
                    if ('wheelDeltaX' in event) {
                        sX = -event.wheelDeltaX / 120;
                    }
                    if ('axis' in event && event.axis === event.HORIZONTAL_AXIS) {
                        sX = sY;
                        sY = 0;
                    }
                    pX = sX * PIXEL_STEP;
                    pY = sY * PIXEL_STEP;
                    if ('deltaY' in event) {
                        pY = event.deltaY;
                    }
                    if ('deltaX' in event) {
                        pX = event.deltaX;
                    }
                    if ((pX || pY) && event.deltaMode) {
                        if (event.deltaMode == 1) {
                            pX *= LINE_HEIGHT;
                            pY *= LINE_HEIGHT;
                        } else {
                            pX *= PAGE_HEIGHT;
                            pY *= PAGE_HEIGHT;
                        }
                    }
                    if (pX && !sX) {
                        sX = pX < 1 ? -1 : 1;
                    }
                    if (pY && !sY) {
                        sY = pY < 1 ? -1 : 1;
                    }
                    return {
                        spinX: sX,
                        spinY: sY,
                        pixelX: pX,
                        pixelY: pY
                    };
                }
                normalizeWheel.getEventType = function() {
                    return UserAgent_DEPRECATED.firefox() ? 'DOMMouseScroll' : isEventSupported('wheel') ? 'wheel' : 'mousewheel';
                };
            }();
            $.fn.smoothWheel = function() {
                var options = jQuery.extend({}, arguments[0]);
                return this.each(function(index, elm) {
                    if (!('ontouchstart' in window)) {
                        container = $(this);
                        container.bind("mousewheel", onWheel);
                        container.bind("DOMMouseScroll", onWheel);
                        targetY = oldY = container.scrollTop();
                        currentY = -targetY;
                        minScrollTop = container.get(0).clientHeight - container.get(0).scrollHeight;
                        if (options.onRender) {
                            onRenderCallback = options.onRender;
                        }
                        if (options.remove) {
                            console.log('Mousewheel Removed');
                            running = false;
                            container.unbind("mousewheel", onWheel);
                            container.unbind("DOMMouseScroll", onWheel);
                        } else if (!running) {
                            running = true;
                            animateLoop();
                        }
                    }
                });
            };
            var isMac = navigator.userAgent.indexOf('Mac OS X') != -1;
            var isFirefox = navigator.userAgent.indexOf('Firefox') != -1;
            if (!isMac && !isFirefox) {
                $(document).smoothWheel();
            }
            return this;
        }
    };
    $.fn.rellaSmoothMouseWheel = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new SmoothMouseWheel(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $("body.smooth-wheel-enabled").rellaSmoothMouseWheel();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    if (window.xMode) {
        return;
    }
    var instanceName = '__FooterFixed';
    var FooterFixed = function(el, options) {
        return this.init(el, options);
    };
    FooterFixed.defaults = {};
    FooterFixed.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, FooterFixed.defaults, options);
            return this;
        },
        build: function() {
            var element = this.el, mainContents = $('#content'), footerHeight = element.outerHeight() - 1;
            element.addClass('is-fixed');
            mainContents.css({
                marginBottom: footerHeight
            });
        }
    };
    $.fn.RellaFooterFixed = function(settings) {
        return this.map(function() {
            var el = $(this);
            el.imagesLoaded(function() {
                if (el.data(instanceName)) {
                    return el.data(instanceName);
                } else {
                    var pluginOptions = el.data('plugin-options'), opts;
                    if (pluginOptions) {
                        opts = $.extend(true, {}, settings, pluginOptions);
                    }
                    return new FooterFixed(el, opts);
                }
            });
        });
    };
    $(document).ready(function() {
        $('footer[data-fixed]').RellaFooterFixed();
    });
    $(window).on('resize', function() {
        $('footer[data-fixed]').RellaFooterFixed();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    if (window.xMode) {
        return;
    }
    var instanceName = '__Hover3d';
    var Hover3d = function(el, options) {
        return this.init(el, options);
    };
    Hover3d.defaults = {};
    Hover3d.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, Hover3d.defaults, options);
            return this;
        },
        build: function() {
            'use strict';
            window.ATicon = {};
            window.ATicon.getInstance = function(jQueryDOMElement) {
                jQueryDOMElement.imagesLoaded(function() {
                    if (jQueryDOMElement === null) throw new Error("Passed in element doesn't exist in DOM");
                    return new ATicon(jQueryDOMElement);
                });
            };
            function ATicon(jQueryDomElement) {
                this.$icon = jQueryDomElement;
                if (!this.$icon.length) {
                    return;
                }
                this.perspectiveAmount = 1200;
                this.offset = this.$icon.offset();
                this.width = this.$icon.width();
                this.height = this.$icon.height();
                this.maxRotation = 8;
                this.maxTranslation = 4;
                var that = this;
                this.$icon.on('mousemove', function(e) {
                    that.events.hover.call(that, e);
                }).on('mouseleave', function(e) {
                    that.events.leave.call(that, e);
                });
            }
            ATicon.prototype = {
                appleTvAnimate: function(element, config) {
                    var rotate = "rotateX(" + config.xRotationPercentage * config.maxRotationX + "deg)" + " rotateY(" + config.yRotationPercentage * config.maxRotationY + "deg)";
                    var translate = " translate3d(" + config.xTranslationPercentage * config.maxTranslationX + "px," + config.yTranslationPercentage * config.maxTranslationY + "px, 0px)";
                    TweenMax.to(element, .3, {
                        rotationX: -config.xRotationPercentage * config.maxRotationX,
                        rotationY: -config.yRotationPercentage * config.maxRotationY,
                        x: -config.xTranslationPercentage * config.maxTranslationX,
                        y: -config.yTranslationPercentage * config.maxTranslationY,
                        ease: Linear.easeNone,
                        perspective: this.perspectiveAmount
                    });
                }
            };
            ATicon.prototype.events = {
                hover: function(e) {
                    var that = this;
                    var mouseOffsetInside = {
                        x: e.pageX - this.offset.left,
                        y: e.pageY - this.offset.top
                    };
                    that.$icon.addClass('mouse-in');
                    function calculateRotationPercentage(offset, dimension) {
                        return -2 / dimension * offset + 1;
                    }
                    function calculateTranslationPercentage(offset, dimension) {
                        return -2 / dimension * offset + 1;
                    }
                    var xRotationPercentage = calculateRotationPercentage(mouseOffsetInside.y, this.height);
                    var yRotationPercentage = calculateRotationPercentage(mouseOffsetInside.x, this.width) * -1;
                    var xTranslationPercentage = calculateTranslationPercentage(mouseOffsetInside.x, this.width);
                    var yTranslationPercentage = calculateTranslationPercentage(mouseOffsetInside.y, this.height);
                    this.$icon.find('[data-stacking-factor]').each(function(index, element) {
                        var stackingFactor = $(element).attr('data-stacking-factor');
                        that.appleTvAnimate($(element), {
                            maxTranslationX: that.maxTranslation * stackingFactor,
                            maxTranslationY: that.maxTranslation * stackingFactor,
                            maxRotationX: that.maxRotation * stackingFactor,
                            maxRotationY: that.maxRotation * stackingFactor,
                            xRotationPercentage: xRotationPercentage,
                            yRotationPercentage: yRotationPercentage,
                            xTranslationPercentage: xTranslationPercentage,
                            yTranslationPercentage: yTranslationPercentage
                        });
                    });
                },
                leave: function(e) {
                    var that = this;
                    that.$icon.removeClass('mouse-in');
                    this.$icon.find('[data-stacking-factor]').each(function(index, element) {
                        that.appleTvAnimate($(element), {
                            maxTranslationX: 0,
                            maxTranslationY: 0,
                            maxRotationX: 0,
                            maxRotationY: 0,
                            xRotationPercentage: 0,
                            yRotationPercentage: 0,
                            xTranslationPercentage: 0,
                            yTranslationPercentage: 0
                        });
                    });
                }
            };
        }
    };
    $.fn.RellaHover3d = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-hover3d-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new Hover3d(el, opts);
            }
        });
    };
    if ($(window).width() >= 992) {
        $(window).on('load resize', function() {
            $(document).RellaHover3d();
            $('[data-hover3d]').each(function() {
                ATicon.getInstance($(this));
            });
        });
    }
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    var instanceName = '__ProgressiveLoad';
    var ProgressiveLoad = function(el, options) {
        return this.init(el, options);
    };
    ProgressiveLoad.defaults = {};
    ProgressiveLoad.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, ProgressiveLoad.defaults, options);
            return this;
        },
        debounce: function(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this, args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        },
        build: function() {
            var self = this, progressiveImage = self.el;
            if (typeof progressively === typeof undefined) {
                return;
            }
            setAspectRatio();
            vcCares();
            function setAspectRatio(el) {
                progressiveImage.each(function() {
                    var $this = $(this), elementRowParent = $this.closest('.row');
                    var onImagesLoad = self.debounce(function() {
                        $.fn.matchHeight._update();
                    }, 1e3);
                    $this.imagesLoaded(function() {
                        if ($this.closest('[data-mh]').length) {
                            onImagesLoad();
                        }
                    });
                    if (typeof elementRowParent !== typeof undefined || elementRowParent !== null) {
                        elementRowParent.imagesLoaded(function() {
                            if (elementRowParent.data('isotope')) {
                                elementRowParent.isotope('layout');
                            }
                            if ($('[data-mh]').length) {
                                $.fn.matchHeight._afterUpdate = function(event, groups) {
                                    if (elementRowParent.data('isotope')) {
                                        elementRowParent.isotope('layout');
                                    }
                                };
                            }
                        });
                    }
                });
            }
            function vcCares() {
                $(progressiveImage).each(function() {
                    var $this = $(this), vcParent = $this.closest('.wpb_single_image').addClass('wpb_single_image_progressive');
                    if (vcParent.length) {
                        if (vcParent.is(':only-child') || vcParent.width() === vcParent.parent().width()) {
                            vcParent.find('.vc_figure').css('display', 'block');
                            vcParent.find('.vc_single_image-wrapper').css('display', 'block');
                            $(this).addClass('width-auto');
                            setAspectRatio();
                        }
                    }
                });
            }
            var progressive = progressively.init({
                throttle: 50,
                delay: 30,
                onLoad: function(el) {
                    $(el).parent().addClass('progressive-image--is-loaded');
                    if ($(el).closest('.portfolio-main-image').length) {
                        $(el).closest('.portfolio-main-image').addClass('progressive-image--is-loaded');
                    }
                    if ($(el).closest('.carousel-items').length && $(el).closest('.carousel-items').data('flickity')) {
                        $(el).closest('.carousel-items').flickity('resize');
                    }
                    setTimeout(function() {
                        if ($(el).closest('[data-plugin-masonry]').length && $(el).closest('[data-plugin-masonry]').data('isotope')) {
                            $(el).closest('[data-plugin-masonry]').isotope('layout');
                        }
                    }, 300);
                }
            });
            $('body').addClass('progressive-load--activated');
            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function() {
                if (typeof progressively !== typeof undefined) {
                    progressively.render();
                }
            });
            $(document).on('shown.bs.collapse', '.collapse', function() {
                if (typeof progressively !== typeof undefined) {
                    progressively.render();
                }
            });
        }
    };
    $.fn.RellaProgressiveLoad = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new ProgressiveLoad(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $('.progressive__img').RellaProgressiveLoad();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    var instanceName = '__MobileNav';
    var MobileNav = function(el, options) {
        return this.init(el, options);
    };
    MobileNav.defaults = {};
    MobileNav.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, MobileNav.defaults, options);
            return this;
        },
        build: function() {
            var mainHeader = $('.main-header'), mainNav = mainHeader.find('.main-nav'), submenu = mainNav.find('.nav-item-children');
            if ($(window).width() <= 991) {
                mainHeader.addClass('mobile').removeClass('desktop');
                submenu.parent().off('mouseenter mouseleave');
                submenu.css({
                    overflow: '',
                    visibility: '',
                    opacity: '',
                    height: '',
                    transform: ''
                });
            } else {
                mainHeader.removeClass('mobile').addClass('desktop');
            }
        }
    };
    $.fn.RellaMobileNav = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new MobileNav(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $(document).RellaMobileNav();
    });
    $(window).on('resize', function() {
        $(document).RellaMobileNav();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    var instanceName = '__Header';
    var Header = function(el, options) {
        return this.init(el, options);
    };
    Header.defaults = {};
    Header.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, Header.defaults, options);
            return this;
        },
        build: function() {
            this.resizeWindow();
            this.headerModules();
            this.megamenu();
            this.desktopHeaderSubmenu();
            this.mobileHeaderSubmenu();
            this.headerSticky();
        },
        headerModules: function() {
            var self = this, mainHeader = $('.main-header'), mainBar = mainHeader.find('.main-bar'), secondaryBar = mainHeader.find('.secondary-bar'), module = mainHeader.find('.header-module'), navbarCollapse = mainHeader.find('.navbar-collapse'), transformPrefixed = Modernizr.prefixedCSS('transform'), navToggle = mainHeader.find('.navbar-toggle').not('.module-toggle'), navTopBar = $('<div class="nav-top-bar"></div>'), mobileModules = $('.mobile-header-container'), ghostSearhSelectors = [], isRTL = $('html').attr('dir') == 'rtl';
            if (typeof TimelineMax != 'undefined' || typeof TimelineMax == 'function') {
                var itemsTimeline = new TimelineMax({
                    callbackScope: module
                }), timelineTotalDuration = 0;
            }
            if (navToggle.length) {
                navToggle.each(function() {
                    var $this = $(this).wrap('<div class="header-module module-nav-trigger hidden-lg hidden-md d-lg-none d-xl-none"></div>'), toggleParent = $this.parents('.main-bar') || $this.parents('.secondary-bar');
                    $this.attr('data-target', '#mobile-nav');
                    if (!toggleParent.children('.header-module.module-nav-trigger.hidden-lg.hidden-md').length) {
                        $this.parent().appendTo(toggleParent);
                    }
                });
            }
            mainBar.each(function() {
                var $this = $(this), headerModule = $this.find('.header-module');
                $this.find('.navbar-header').prependTo($this);
            });
            $('.module-fullheight-side').appendTo('body');
            if (!$('body').children('.main-header.clone').length) {
                var mainHeaderClone = $('.main-header').clone(true).addClass('clone'), mainNav = $('.main-header').find('.main-nav'), navbarCollapseClone = mainNav.parent('.navbar-collapse').clone(true).attr('aria-expanded', 'false');
                navbarCollapseClone.attr('id', 'mobile-nav');
                mainHeaderClone.children().remove();
                navbarCollapseClone.children().wrapAll('<div class="navbar-collapse-inner"></div>');
                navbarCollapseClone.appendTo(mainHeaderClone);
                mainHeaderClone.appendTo('body');
                if (mobileModules.length) {
                    mobileModules.appendTo($('.navbar-collapse', mainHeaderClone).first());
                }
            }
            if (mainHeader.hasClass('mobile')) {
                navbarCollapseClone.on('show.bs.collapse', function() {
                    $('body').addClass('mobile-nav-is-showing');
                }).on('hide.bs.collapse', function() {
                    $('body').removeClass('mobile-nav-is-showing');
                });
                navbarCollapseClone.on('shown.bs.collapse', function() {
                    document.querySelector('.main-header.clone .navbar-collapse').addEventListener('touchstart', handleTouchStart, false);
                    document.querySelector('.main-header.clone .navbar-collapse').addEventListener('touchmove', handleTouchMove, false);
                    var xDown = null;
                    var yDown = null;
                    function handleTouchStart(evt) {
                        xDown = evt.touches[0].clientX;
                        yDown = evt.touches[0].clientY;
                    }
                    function handleTouchMove(evt) {
                        if (!xDown || !yDown) {
                            return;
                        }
                        var xUp = evt.touches[0].clientX;
                        var yUp = evt.touches[0].clientY;
                        var xDiff = xDown - xUp;
                        var yDiff = yDown - yUp;
                        if (Math.abs(xDiff) > Math.abs(yDiff)) {
                            console.log(xDiff);
                            if (xDiff >= 11) {
                                navbarCollapseClone.collapse('hide');
                            }
                        }
                        xDown = null;
                        yDown = null;
                    }
                });
            }
            module.each(function() {
                var $this = $(this), moduleContainer = $this.find('.module-container'), moduleTrigger = $this.find('.module-trigger'), dataTarget = moduleTrigger.data('target'), sectionParent = $this.closest('.vc_section');
                if ($('.masonry-filters').length && $this.find('.masonry-filters-clone').length) {
                    $('.masonry-filters').clone('true').appendTo($this.find('.masonry-filters-clone'));
                }
                if (typeof dataTarget === typeof undefined || dataTarget === null) {
                    $this.off('mouseenter mouseleave');
                    $this.off('mouseenter mouseleave', '.module-trigger');
                    $this.on('mouseenter', '.module-trigger', function() {
                        moduleContainer.stop().slideDown(300);
                        $this.addClass('module-container-is-showing');
                        sectionParent.addClass('module-is-showing');
                        if ($this.find('input[type="search"]').length) {
                            setTimeout(function() {
                                $this.find('input[type="search"]').focus();
                            }, 250);
                        }
                        if (typeof progressively !== typeof undefined && $(this).find('.progressive__img').length) {
                            var enableProgressiveLoad = new RellaProgressiveAspectRatio();
                            enableProgressiveLoad.init();
                            progressively.render();
                        }
                    }).on('mouseleave', function() {
                        moduleContainer.stop().slideUp(300);
                        $this.removeClass('module-container-is-showing');
                        sectionParent.removeClass('module-is-showing');
                        if ($this.find('input[type="search"]').length) {
                            setTimeout(function() {
                                $this.find('input[type="search"]').blur();
                            }, 150);
                        }
                    });
                } else {
                    moduleTrigger.off('click');
                    moduleTrigger.on('click', function(event) {
                        var dataTarget = $(this).data('target');
                        event.preventDefault();
                        if ($('body').hasClass('smooth-wheel-enabled') && $(dataTarget).hasClass('navbar-collapse')) {
                            $(document).smoothWheel({
                                remove: true
                            });
                        }
                        if ($this.closest('.module-search-form').length) {
                            var moduleClassList = $this.closest('.module-search-form').attr('class').split(' '), classesToBody = [ 'search-module-is-showing' ];
                            for (var i = 0; i < moduleClassList.length; i++) {
                                if (moduleClassList[i].match('style-')) classesToBody.push('search-module-' + moduleClassList[i]);
                            }
                            $('body').addClass(classesToBody.join(' '));
                            $this.find('input[type="search"]').on('keyup', function() {
                                if ($this.find('input[type="search"]').val() != '') {
                                    $this.addClass('input-filled');
                                } else {
                                    $this.removeClass('input-filled');
                                }
                            });
                        }
                        if (!$this.closest('.style-ghost').length) {
                            $(dataTarget).toggleClass('is-active');
                            $('[data-target="' + dataTarget + '"]').parent('.header-module').toggleClass('module-container-is-showing');
                            sectionParent.toggleClass('module-is-showing');
                            if ($('.modules-fullscreen').has(this).length) {
                                $('html').toggleClass('overflow-hidden');
                                $('.main-header').toggleClass('module-is-showing');
                                $('body').toggleClass('header-module-is-showing');
                            }
                            if ($this.find('input[type="search"]').length && $this.closest('.module-container-is-showing').length) {
                                setTimeout(function() {
                                    $this.find('input[type="search"]').focus();
                                }, 250);
                            }
                        } else {
                            var siblingModules = $this.prevAll('div'), searchSiblings = [];
                            if ($this.closest('.modules-container').siblings('.navbar-collapse').find('.main-nav').children('li').length) {
                                searchSiblings.push($this.closest('.modules-container').siblings('.navbar-collapse').find('.main-nav').children());
                            }
                            if ($this.closest('.modules-container').siblings('.navbar-brand').length) {
                                if (!isRTL) {
                                    $this.find('.search-form').css({
                                        marginLeft: $this.closest('.modules-container').siblings('.navbar-brand').outerWidth()
                                    });
                                } else {
                                    $this.find('.search-form').css({
                                        marginRight: $this.closest('.modules-container').siblings('.navbar-brand').outerWidth()
                                    });
                                }
                            }
                            siblingModules.each(function() {
                                var $this = $(this);
                                if ($this.find('li').length) {
                                    searchSiblings.push($this.find('li'));
                                } else {
                                    searchSiblings.push($this);
                                }
                            });
                            $.each(searchSiblings, function(i, sibling) {
                                $.each(sibling, function(i, selector) {
                                    ghostSearhSelectors.push(selector);
                                });
                            });
                            if (typeof TimelineMax != 'undefined' || typeof TimelineMax == 'function') {
                                itemsTimeline.staggerTo($(ghostSearhSelectors).get().reverse(), .3, {
                                    scale: .5,
                                    opacity: 0,
                                    ease: Power3.easeIn
                                }, .04);
                                timelineTotalDuration = timelineTotalDuration == 0 ? itemsTimeline.duration() : timelineTotalDuration;
                                itemsTimeline.pause();
                                if (itemsTimeline.progress() < 1) {
                                    itemsTimeline.play();
                                    setTimeout(function() {
                                        $this.addClass('module-container-is-showing');
                                        sectionParent.addClass('module-is-showing');
                                    }, timelineTotalDuration * 700);
                                    setTimeout(function() {
                                        $this.find('input[type="search"]').focus();
                                    }, timelineTotalDuration * 1e3);
                                }
                                $this.find('.module-container').find('.module-trigger').on('click', function() {
                                    if (itemsTimeline.progress() > 0 && itemsTimeline.reversed() === false) {
                                        itemsTimeline.seek(timelineTotalDuration).reverse();
                                        setTimeout(function() {
                                            TweenMax.set($(ghostSearhSelectors), {
                                                clearProps: 'all'
                                            });
                                        }, timelineTotalDuration * 1100);
                                    }
                                });
                            }
                        }
                    });
                    if (!$(dataTarget).children('.module-nav-trigger').length && $this.is('.module-nav-trigger')) {
                        $this.clone('true').appendTo(dataTarget);
                    }
                    if ($this.siblings('.modules-container').length && $this.closest('.modules-fullscreen:not([class*=modules-fullscreen-alt-])').length) {
                        $this.siblings('.modules-container').appendTo(dataTarget);
                    }
                    $this.find('.module-container').find('.module-trigger').off('click');
                    $this.find('.module-container').find('.module-trigger').on('click', function() {
                        $this.removeClass('module-container-is-showing');
                        sectionParent.removeClass('module-is-showing');
                        $this.find('input[type="search"]').blur();
                        $('body').removeClass('search-module-is-showing');
                        $('html').removeClass('overflow-hidden');
                    });
                }
            });
            if ($('.main-bar-container.modules-fullscreen').not('.modules-fullscreen-alt-2, .modules-fullscreen-alt-3').length && !$('.navbar-collapse').siblings('.header-module').not('.module-nav-trigger').length) {
                $('.main-bar-container, .main-header').addClass('width-auto');
            }
            if ($('.main-bar-container.modules-fullscreen-alt-3').length && $('.navbar-collapse').length) {
                var mask = $('<div class="header-mask"><div class="mask-inner"></div></div>\x3c!-- /.header-mask --\x3e');
                mask.prependTo('.navbar-collapse');
            }
            function closeAllModules(event) {
                if ($('body').hasClass('smooth-wheel-enabled') && $('html').hasClass('overflow-hidden')) {
                    $(document).smoothWheel({
                        remove: true
                    });
                    console.log('smooth mousewheel initialized');
                    $(document).smoothWheel();
                }
                module.find('.module-container').stop().slideUp(300);
                module.removeClass('module-container-is-showing');
                module.closest('.main-bar-container, .secondary-bar').removeClass('module-is-showing');
                if (module.find('input[type="search"]').length) {
                    setTimeout(function() {
                        module.find('input[type="search"]').blur();
                    }, 150);
                }
                module.find('.is-active').removeClass('is-active');
                $('html').removeClass('overflow-hidden');
                $('body').removeClass('header-module-is-showing search-module-is-showing');
                $('.main-header').removeClass('module-is-showing');
                module.not('.style-ghost').removeClass('module-container-is-showing is-active');
                $('.module-container-is-showing').removeClass('module-container-is-showing');
                module.find('input[type="search"]').blur();
                $('.navbar-collapse').removeClass('is-active');
                $('.module-nav-trigger').removeClass('dl-active');
                if (typeof TimelineMax != 'undefined' || typeof TimelineMax == 'function') {
                    if (itemsTimeline.progress() > 0 && itemsTimeline.reversed() === false) {
                        $('.header-module.style-ghost').removeClass('module-container-is-showing');
                        sectionParent.removeClass('module-is-showing');
                        itemsTimeline.seek(timelineTotalDuration).reverse();
                        setTimeout(function() {
                            TweenMax.set($(ghostSearhSelectors), {
                                clearProps: 'all'
                            });
                        }, timelineTotalDuration * 1100);
                    }
                }
            }
            function closeMobileNav(event) {
                $('.navbar-collapse').attr('aria-expanded', 'false').addClass('collapsing').removeClass('in');
                navToggle.attr('aria-expanded', 'false').addClass('collapsed');
            }
            $(document).on('click touchend', function(e) {
                var target = $(e.target);
                if (!module.is(target) && !module.has(target).length && !target.has('.modules-fullscreen').length || target.parents().is('.module-trigger-inner')) {
                    closeAllModules(e);
                }
                if (!navToggle.is(target) && !navToggle.parent().is(target) && !$('.navbar-collapse').is(target) && !$('.navbar-collapse').has(target).length && $('.navbar-collapse').hasClass('in')) {
                    $('.navbar-toggle', '.module-nav-trigger').trigger('click');
                }
            });
            $(document).on('keyup', function(e) {
                if (e.keyCode == 27) {
                    closeAllModules(e);
                }
            });
            $('.module-inner + .module-nav-trigger .module-trigger').on('click', function(e) {
                closeAllModules(e);
            });
        },
        megamenu: function() {
            var el = $('.megamenu');
            el.each(function() {
                var liParent = $(this);
                var megamenu = liParent.children('.nav-item-children'), megamenuContainer = liParent.find('.nav-item-children > li > .container'), megamenuColumns, megamenuColumnsWidth = 0, parentPos = liParent.offset().left + liParent.outerWidth(), parentContainer = liParent.closest('.container').length ? liParent.closest('.container') : liParent.closest('.container-fluid'), megamenuPos;
                if (megamenuContainer.children('.vc_row').children('.wpb_column').length) {
                    megamenuColumns = megamenuContainer.children('.vc_row').children('.wpb_column');
                } else {
                    megamenuColumns = megamenuContainer.children('.row').children('.megamenu-column');
                }
                liParent.addClass('columns-' + megamenuColumns.length);
                megamenuColumns.each(function() {
                    megamenuColumnsWidth += $(this).outerWidth();
                });
                megamenuContainer.css('width', '');
                liParent.removeClass('width-applied');
                if (liParent.closest('.main-bar-side').length) {
                    if (megamenuContainer.offset().left + megamenuContainer.outerWidth() >= $(window).width()) {
                        megamenuContainer.width('');
                        megamenuContainer.width($(window).width() - $('.main-bar-container').outerWidth());
                    }
                }
                megamenuPos = parentPos - megamenu.outerWidth() / 2 < 0 ? 0 : parentPos - megamenu.outerWidth() / 2;
                megamenu.css({
                    display: 'block',
                    visibility: 'hidden',
                    opacity: 0
                });
                megamenu.show().css({
                    left: '',
                    right: ''
                });
                megamenu.css('left', megamenuPos);
                if (megamenuPos + megamenuColumnsWidth >= parentContainer.outerWidth()) {
                    megamenu.css({
                        left: 'auto',
                        right: -15
                    });
                } else if (parentPos <= 0) {
                    megamenu.css({
                        left: -15,
                        right: 'auto'
                    });
                }
                setTimeout(function() {
                    megamenuContainer.width('');
                    megamenuContainer.width(megamenuColumnsWidth);
                    liParent.addClass('width-applied');
                    megamenu.css({
                        display: 'none',
                        visibility: 'visible',
                        opacity: 1
                    });
                }, 650);
            });
        },
        desktopHeaderSubmenu: function() {
            var mainHeader = $('.main-header'), mainBarContainer = mainHeader.find('.main-bar-container'), mainNav = mainHeader.find('.main-nav'), submenu = mainNav.find('.nav-item-children, .children'), megamenuParent = mainNav.find('.megamenu'), isRTL = $('html').attr('dir') == 'rtl';
            mainBarContainer.each(function() {
                var $this = $(this);
                if ($this.find('.module-search-form.style-offcanvas').length) {
                    $this.find('.module-search-form.style-offcanvas').each(function() {
                        var offCanvasSearch = $(this);
                        offCanvasSearch.children('.module-container').css({
                            right: parseInt(Math.abs($(window).innerWidth() - (offCanvasSearch.offset().left + offCanvasSearch.width()))) * -1,
                            top: parseInt(Math.abs(offCanvasSearch.offset().top)) * -1
                        });
                    });
                }
                if ($this.hasClass('modules-fullscreen')) {
                    $('.modules-fullscreen').find('.main-nav').dlmenu({
                        animationClasses: {
                            classin: 'dl-animate-in-2',
                            classout: 'dl-animate-out-2'
                        }
                    });
                    if (!$('.modules-fullscreen').find('.main-nav').parent('.main-nav-container').length) {
                        $('.modules-fullscreen').find('.main-nav').wrap('<div class="main-nav-container"></div>');
                    }
                    var mainBar = $('.main-bar'), navbarCollapse = mainBar.find('.navbar-collapse'), mainBarOffset = mainBar.offset();
                    if (navbarCollapse.length) {
                        navbarCollapse.css({
                            top: '',
                            left: '',
                            right: ''
                        });
                        navbarCollapse.closest('.wpb_column').imagesLoaded(function() {
                            navbarCollapse.css({});
                        });
                    }
                    if ($('.module-search-form.style-fullscreen').length) {
                        $('.module-search-form.style-fullscreen .module-container').css({
                            top: '',
                            left: '',
                            right: ''
                        });
                        navbarCollapse.closest('.wpb_column').imagesLoaded(function() {
                            $('.module-search-form.style-fullscreen .module-container').css({});
                        });
                    }
                    $(window).on('resize', function() {
                        if (navbarCollapse.length) {
                            navbarCollapse.css({
                                top: '',
                                left: '',
                                right: ''
                            });
                            navbarCollapse.css({});
                        }
                        if ($('.module-search-form.style-fullscreen').length) {
                            $('.module-search-form.style-fullscreen .module-container').css({
                                top: '',
                                left: '',
                                right: ''
                            });
                            navbarCollapse.closest('.wpb_column').imagesLoaded(function() {
                                $('.module-search-form.style-fullscreen .module-container').css({});
                            });
                        }
                    });
                }
                if ($this.hasClass('modules-fullscreen-alt')) {
                    $this.find('.navbar-collapse').children().wrapAll('<div class="container"></div>');
                }
            });
            if (mainHeader.hasClass('mobile') || mainBarContainer.hasClass('modules-fullscreen')) {
                return;
            }
            if (mainHeader.find('.custom-menu').find('.sub-menu').length) {
                submenu.add(mainHeader.find('.custom-menu').find('.sub-menu'));
            }
            submenu.add(mainHeader.find('.custom-menu').find('.sub-menu')).each(function() {
                var $this = $(this), subParent = $this.parent();
                if (!isRTL) {
                    if ($this.offset().left + $this.outerWidth() >= $(window).width() && !subParent.is('.megamenu')) {
                        $this.addClass('to-right');
                    } else {
                        $this.removeClass('to-right');
                    }
                } else {
                    if ($this.offset().left <= 0 && !subParent.is('.megamenu')) {
                        $this.addClass('to-right');
                    } else {
                        $this.removeClass('to-right');
                    }
                }
                setTimeout(function() {
                    $this.hide();
                }, 50);
                if (!window.xMode) {
                    subParent.on('mouseenter', function() {
                        var mainHeader = $('.main-header');
                        if (mainHeader.hasClass("desktop")) {
                            var $liParent = $(this), navChild = $liParent.children('.nav-item-children, .sub-menu, .children');
                            if (navChild.length) {
                                navChild.css({
                                    visibility: 'visible',
                                    opacity: 1
                                });
                                $liParent.addClass('submenu-is-showing');
                                navChild.stop().fadeIn(200);
                            }
                        }
                        if (typeof progressively !== typeof undefined && $(this).find('.progressive__img').length) {
                            var enableProgressiveLoad = new RellaProgressiveAspectRatio();
                            enableProgressiveLoad.init();
                            progressively.render();
                        }
                    }).on('mouseleave', function() {
                        var mainHeader = $('.main-header');
                        if (mainHeader.hasClass("desktop")) {
                            var $liParent = $(this), navChild = $liParent.children('.nav-item-children, .sub-menu, .children');
                            if (navChild.length) {
                                $liParent.removeClass('submenu-is-showing');
                                navChild.stop().fadeOut(100);
                            }
                        }
                    });
                } else {
                    var arrow = $('<i class="fa fa-angle-down xmode-toggle"></i>');
                    subParent.children('a').append(arrow);
                    subParent.on('click', '> a', function() {
                        var mainHeader = $('.main-header');
                        if (mainHeader.hasClass("desktop")) {
                            var $liParent = $(this).parent(), navChild = $liParent.children('.nav-item-children');
                            if (navChild.length) {
                                $liParent.toggleClass('submenu-is-showing').siblings().removeClass('submenu-is-showing').find('.nav-item-children').hide();
                                navChild.stop().toggle(100);
                            }
                        }
                    });
                }
                if (submenu.closest('.modules-fullscreen').length) {
                    submenu.parent().off('mouseenter mouseleave');
                }
            });
        },
        responsiveMenu: function(status) {
            var self = this, mainHeader = $(".main-header"), mainNav = mainHeader.find('.main-nav'), submenu = mainNav.find('.nav-item-children, .children');
            if (status) {
                submenu.each(function() {});
            } else {
                submenu.each(function() {});
            }
        },
        resizeWindow: function(status) {
            var mainHeader = $('.main-header'), windowWidth = $("body").prop("scrollWidth"), fullWidthRow = mainHeader.find('.main-bar').children('[data-vc-full-width]'), flexBasis = Modernizr.prefixedCSS('flex-basis'), self = this;
            if (windowWidth > 991) {
                mainHeader.addClass("desktop");
                mainHeader.removeClass("mobile");
                setTimeout(function() {
                    fullWidthRow.each(function() {
                        $(this).css(flexBasis, $(this).next('.vc_row-full-width').width() + $(this).next('.vc_row-full-width').offset().left * 2);
                    });
                }, 250);
                self.responsiveMenu(false);
            } else {
                mainHeader.removeClass("desktop");
                mainHeader.addClass("mobile");
                setTimeout(function() {
                    fullWidthRow.each(function() {
                        $(this).css(flexBasis, '');
                    });
                }, 250);
                self.responsiveMenu(true);
            }
            $(window).on('load resize', function() {
                windowWidth = $(window).width();
                if (windowWidth > 991) {
                    mainHeader.addClass("desktop");
                    mainHeader.removeClass("mobile");
                    setTimeout(function() {
                        fullWidthRow.each(function() {
                            $(this).css(flexBasis, $(this).next('.vc_row-full-width').width() + $(this).next('.vc_row-full-width').offset().left * 2);
                        });
                    }, 250);
                    self.responsiveMenu(false);
                    $('.megamenu .nav-item-children', mainHeader).css({
                        display: 'block',
                        visibility: 'hidden',
                        opacity: 0
                    });
                    self.megamenu();
                } else {
                    mainHeader.removeClass("desktop");
                    mainHeader.addClass("mobile");
                    setTimeout(function() {
                        fullWidthRow.each(function() {
                            $(this).css(flexBasis, '');
                        });
                    }, 250);
                    self.responsiveMenu(true);
                }
            });
        },
        mobileHeaderSubmenu: function() {
            var mainNav = $('.main-header.mobile').find('.main-nav'), submenu = mainNav.find('.nav-item-children, .children');
            $('body').on('click', '.main-header.mobile .main-nav li > a', function(e) {
                if (!$(this).siblings('.nav-item-children, .children').length) {
                    return;
                }
                e.preventDefault();
                var submenu = $(this).siblings('.nav-item-children, .children');
                submenu.stop().slideToggle();
                $(this).parent().toggleClass('submenu-is-showing');
                $(this).parent().siblings().find('.nav-item-children, .children').slideUp(300).end().removeClass('submenu-is-showing');
            });
        },
        headerSticky: function() {
            var el = $('[data-sticky]');
            if ($(window).width() >= 768) {
                if (!el.length) {
                    return;
                }
                el.each(function() {
                    var $this = $(this).wrap('<div class="sticky-placeholder"></div>'), stickyPlaceholder = $this.parent(), mainBar = $this.find('.main-bar'), mainBarOffset = mainBar.offset(), navbarCollapse = $('.main-header').find('.navbar-collapse'), transformPrefixed = Modernizr.prefixedCSS('transform'), offsetEl = $($this.data('sticky-offset')), offsetElOffsetTop = 0, offsetElHeight = 0, elementOffsetTop = 0, onStickyOffset = 0, stickyType = $this.data(), placeholderType = $this.is('.main-bar-container') ? 'main-bar-placeholder' : 'secondary-bar-placeholder', newClassNames, prevClassNames, hr;
                    stickyPlaceholder.addClass(Object.keys(stickyType).join(' ') + ' ' + placeholderType).removeClass('sticky');
                    stickyPlaceholder.attr({
                        'data-height': $this.outerHeight(),
                        'data-sticky-offset-top': 0
                    });
                    var getClassames = function(element) {
                        return element.attr('class');
                    };
                    var switchClasses = function(searchIn, searchFor, targetClassname) {
                        if (searchIn.search(searchFor) >= 0) {
                            $this.addClass(targetClassname);
                        } else {
                            $this.removeClass(targetClassname);
                        }
                    };
                    var getStickyOffset = function() {
                        var prevEl = stickyPlaceholder.prevAll('.sticky-placeholder').children(), prevElHeight = parseInt(prevEl.parent().attr('data-height'), 10) || 0, prevElOffset = prevElHeight + parseInt(prevEl.parent().attr('data-sticky-offset-top'), 10) || 0;
                        return prevElOffset;
                    };
                    if (offsetEl.length && offsetEl.is(':visible')) {
                        offsetEl.each(function() {
                            var $this = $(this);
                            $this.imagesLoaded(function() {
                                offsetElHeight += $this.outerHeight();
                                offsetElOffsetTop += $this.offset().top;
                            });
                        });
                    } else {
                        offsetElHeight = 0;
                    }
                    $this.imagesLoaded(function() {
                        if (!$this.closest('.titlebar[data-enable-fullheight]').length) {
                            stickyPlaceholder.not('.onlyVisibleOnsticky').height($this.outerHeight());
                        }
                        stickyPlaceholder.attr({
                            'data-height': '',
                            'data-sticky-offset-top': 0
                        });
                        stickyPlaceholder.attr({
                            'data-height': $this.outerHeight(),
                            'data-sticky-offset-top': getStickyOffset()
                        });
                        hr = new Headroom($this.get(0), {
                            tolerance: {
                                up: 6,
                                down: 1
                            },
                            offset: $(".main-header").outerHeight(),
                            onPin: function() {
                                stickyPlaceholder.attr({
                                    'data-height': '',
                                    'data-sticky-offset-top': 0
                                });
                                stickyPlaceholder.attr({
                                    'data-height': $this.outerHeight(),
                                    'data-sticky-offset-top': getStickyOffset()
                                });
                                $this.css({
                                    top: ''
                                });
                                $this.css({
                                    top: parseInt(stickyPlaceholder.attr('data-sticky-offset-top'), 10)
                                });
                                prevClassNames = newClassNames;
                                newClassNames = getClassames($this);
                                switchClasses(prevClassNames, 'headroom--top', 'pinned-from-top');
                                if ($this.hasClass('modules-fullscreen')) {
                                    navbarCollapse.css({
                                        top: '',
                                        left: '',
                                        right: ''
                                    });
                                    navbarCollapse.css({});
                                }
                                if ($('.module-search-form.style-fullscreen').length) {
                                    $('.module-search-form.style-fullscreen .module-container').css({
                                        top: '',
                                        left: '',
                                        right: ''
                                    });
                                    navbarCollapse.closest('.wpb_column').imagesLoaded(function() {
                                        $('.module-search-form.style-fullscreen .module-container').css({});
                                    });
                                }
                            },
                            onUnpin: function() {
                                stickyPlaceholder.attr({
                                    'data-height': '',
                                    'data-sticky-offset-top': 0
                                });
                                stickyPlaceholder.attr({
                                    'data-height': $this.outerHeight(),
                                    'data-sticky-offset-top': getStickyOffset()
                                });
                                $this.css({
                                    top: ''
                                });
                                if ($this.is('[data-sticky-always]')) {
                                    $this.css({
                                        top: parseInt(stickyPlaceholder.attr('data-sticky-offset-top'), 10)
                                    });
                                } else {
                                    $this.css({
                                        top: ''
                                    });
                                }
                                prevClassNames = newClassNames;
                                newClassNames = getClassames($this);
                                switchClasses(prevClassNames, 'headroom--top', 'pinned-from-top');
                                if ($this.hasClass('modules-fullscreen')) {
                                    navbarCollapse.css({
                                        top: '',
                                        left: '',
                                        right: ''
                                    });
                                }
                                $this.find('.is-active').removeClass('is-active');
                                $this.find('.module-container-is-showing').not('.style-ghost, .style-fullscreen').removeClass('module-container-is-showing');
                                $('body').not('.search-module-style-ghost, .search-module-style-fullscreen').removeClass('header-module-is-showing search-module-is-showing');
                                $('html').removeClass('overflow-hidden');
                                $('.main-header').removeClass('module-is-showing');
                            },
                            onNotTop: function() {
                                stickyPlaceholder.attr({
                                    'data-height': '',
                                    'data-sticky-offset-top': 0
                                });
                                stickyPlaceholder.attr({
                                    'data-height': $this.outerHeight(),
                                    'data-sticky-offset-top': getStickyOffset()
                                });
                                $this.css({
                                    top: ''
                                });
                                $this.css({
                                    top: parseInt(stickyPlaceholder.attr('data-sticky-offset-top'), 10)
                                });
                            },
                            onTop: function() {
                                $this.css({
                                    top: ''
                                });
                                $this.parent('.onlyVisibleOnsticky').height('');
                                if ($this.hasClass('modules-fullscreen')) {
                                    navbarCollapse.css({
                                        top: '',
                                        left: '',
                                        right: ''
                                    });
                                    navbarCollapse.css({});
                                }
                                if ($('.module-search-form.style-fullscreen').length) {
                                    $('.module-search-form.style-fullscreen .module-container').css({
                                        top: '',
                                        left: '',
                                        right: ''
                                    });
                                    $('.module-search-form.style-fullscreen .module-container').css({});
                                }
                                prevClassNames = newClassNames;
                                newClassNames = getClassames($this);
                            }
                        });
                        hr.init();
                        newClassNames = getClassames($this);
                        $(window).on('resize', function() {
                            if (!$this.closest('.titlebar[data-enable-fullheight]').length) {
                                stickyPlaceholder.not('.onlyVisibleOnsticky').height($this.outerHeight());
                            }
                            hr.offset = $(".main-header").outerHeight();
                        });
                    });
                });
            } else {
                if (!$('body').hasClass('mobile-header-overlay')) {
                    var mainHeader = $('.main-header').not('.clone'), headerHeight = mainHeader.outerHeight(), heightPlaceholder = $('<div class="main-header-placeholder"></div>');
                    heightPlaceholder.height(headerHeight).insertBefore(mainHeader);
                    mainHeader.addClass('is-fixed');
                }
            }
        }
    };
    $.fn.RellaHeader = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new Header(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $(document).RellaHeader();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    var instanceName = '__EnableFitText';
    var EnableFitText = function(el, options) {
        return this.init(el, options);
    };
    EnableFitText.defaults = {};
    EnableFitText.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, EnableFitText.defaults, options);
            return this;
        },
        build: function() {
            var el = $('[data-fitText]');
            if (!el.length) {
                return;
            }
            $.fn.fitText = function(kompressor, options) {
                var compressor = kompressor || 1, settings = $.extend({
                    minFontSize: Number.NEGATIVE_INFINITY,
                    maxFontSize: Number.POSITIVE_INFINITY
                }, options);
                return this.each(function() {
                    var $this = $(this);
                    var resizer = function() {
                        $this.css('font-size', Math.max(Math.min($this.width() / (compressor * 10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
                    };
                    resizer();
                    $(window).on('resize.fittext orientationchange.fittext', resizer);
                });
            };
            el.each(function() {
                var $this = $(this), dataMaxFontSize = $this.attr('data-max-fontSize');
                $this.fitText(1, {
                    maxFontSize: dataMaxFontSize
                });
            });
        }
    };
    $.fn.RellaEnableFitText = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new EnableFitText(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $(document).RellaEnableFitText();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    var instanceName = '__EnableMediaElementJs';
    var EnableMediaElementJs = function(el, options) {
        return this.init(el, options);
    };
    EnableMediaElementJs.defaults = {};
    EnableMediaElementJs.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, EnableMediaElementJs.defaults, options);
            return this;
        },
        build: function() {
            var element = this.el, settings = {};
            if (!element.length) {
                return;
            }
            if (typeof _wpmejsSettings !== 'undefined') {
                settings = $.extend(true, {}, _wpmejsSettings);
            }
            settings.success = settings.success || function(mejs) {
                var autoplay, loop;
                if ('flash' === mejs.pluginType) {
                    autoplay = mejs.attributes.autoplay && 'false' !== mejs.attributes.autoplay;
                    loop = mejs.attributes.loop && 'false' !== mejs.attributes.loop;
                    autoplay && mejs.addEventListener('canplay', function() {
                        mejs.play();
                    }, false);
                    loop && mejs.addEventListener('ended', function() {
                        mejs.play();
                    }, false);
                }
            };
            var rellamejsDefaults = {
                audioHeight: 60,
                audioVolume: 'vertical',
                features: [ 'playpause', 'current', 'progress', 'duration', 'tracks', 'volume' ]
            };
            rellamejsDefaults.success = function(media) {
                media.addEventListener('play', function(e) {
                    var $this = $(e.target), parent;
                    parent = $this.closest('.blog-post').length ? $this.closest('.blog-post') : $this.closest('.mejs-container');
                    parent.addClass('is-playing');
                });
                media.addEventListener('pause', function(e) {
                    var $this = $(e.target), parent;
                    parent = $this.closest('.blog-post').length ? $this.closest('.blog-post') : $this.closest('.mejs-container');
                    parent.removeClass('is-playing');
                });
            };
            if ("undefined" !== typeof _wpmejsSettings) {
                $.extend(_wpmejsSettings, rellamejsDefaults);
            }
            element.not('.mejs-container').filter(function() {
                return !$(this).parent().hasClass('.mejs-mediaelement');
            }).mediaelementplayer(rellamejsDefaults);
        }
    };
    $.fn.RellaEnableMediaElementJs = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new EnableMediaElementJs(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $('video, audio').not('.portfolio-main-video').RellaEnableMediaElementJs();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    var instanceName = '__RellaAjaxLoadMore';
    var AjaxLoadMore = function(el, options) {
        return this.init(el, options);
    };
    AjaxLoadMore.defaults = {};
    AjaxLoadMore.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, AjaxLoadMore.defaults, options);
            return this;
        },
        build: function() {
            var doc = this.el;
            doc.off('click', '[data-plugin-ajaxify]');
            doc.on('click', '[data-plugin-ajaxify]', function(event) {
                event.preventDefault();
                var button = $(this), target = button.attr('href'), opts = $.extend(true, {}, button.data('plugin-options'));
                button.addClass('loading');
                $.ajax({
                    type: 'GET',
                    url: target,
                    complete: function() {
                        button.removeClass('loading');
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        alert(errorThrown);
                    },
                    success: function(data) {
                        var wrapper = $(data).find(opts.wrapper), items = wrapper.find(opts.items), nextPageUrl = $(data).find('[data-plugin-ajaxify="true"]').attr('href'), lastDate = $(".timeline-date").last().text().trim(), timelineRows = $(opts.wrapper).find('.timeline-row'), lastTimelineRow = timelineRows.last(), parentOfNewItems;
                        items.imagesLoaded(function() {
                            if (nextPageUrl && target != nextPageUrl) {
                                button.attr('href', nextPageUrl);
                            } else {
                                button.parent().slideUp(300);
                            }
                            if (items.is('.row:not(.timeline-row)')) {
                                items = items.children('[class*=col-]');
                            }
                            if (items.is('.timeline-row')) {
                                $(items).insertBefore(button.parents('.page-nav'));
                            } else {
                                $(opts.wrapper).append(items.parent('[class*=col-]'));
                            }
                            if ($(opts.wrapper).hasClass('timeline')) {
                                var newFirstRow = items.first(), newFirstRowItems = newFirstRow.find('.masonry-item').not('.mid-bar'), timelineRows = $(opts.wrapper).find('.timeline-row'), newDate = $(".timeline-date", items.first()).text().trim();
                                if (newDate === lastDate) {
                                    newFirstRowItems.appendTo(lastTimelineRow).addClass('item-just-added');
                                    parentOfNewItems = newFirstRowItems.parent();
                                    timelineRows.each(function() {
                                        var $this = $(this);
                                        if (!$this.find('.masonry-item').not('.mid-bar').length) {
                                            $this.remove();
                                        }
                                    });
                                    parentOfNewItems.isotope('appended', newFirstRowItems);
                                    if (typeof progressively !== typeof undefined && $('.progressive__img').length) {
                                        var enableProgressiveLoad = new RellaProgressiveAspectRatio();
                                        enableProgressiveLoad.init();
                                        $('.progressive__img').RellaProgressiveLoad();
                                        setTimeout(function() {
                                            progressively.render();
                                        }, 650);
                                    }
                                    setTimeout(function() {
                                        parentOfNewItems.isotope('layout');
                                    }, 300);
                                    parentOfNewItems.on('layoutComplete', function() {
                                        setTimeout(function() {
                                            parentOfNewItems.find('.masonry-item').removeClass('item-just-added');
                                        }, 300);
                                    });
                                }
                            }
                            if (typeof progressively !== typeof undefined && $('.progressive__img').length) {
                                var enableProgressiveLoad = new RellaProgressiveAspectRatio();
                                enableProgressiveLoad.init();
                                $('.progressive__img').RellaProgressiveLoad();
                                setTimeout(function() {
                                    progressively.render();
                                }, 650);
                            }
                            $('[data-plugin-masonry]').rellaMasonryLayout();
                            $('video, audio').RellaEnableMediaElementJs();
                            $(document).RellaCarousel();
                            $(document).RellaOffsetTop();
                            $('[data-parallax="true"]').RellaParallax();
                            $('[data-parallax-bg="true"]').RellaParallaxBG();
                            $('[data-panr]').rellaPanr();
                            $('.lightbox-link').rellaLightbox();
                            if ($('[data-mh]').length && !button.parents('.masonry-creative').length) {
                                $('[data-mh]').matchHeight({
                                    remove: true
                                });
                                $('[data-mh]').matchHeight();
                            }
                            if ($(opts.wrapper).data('isotope')) {
                                if (!items.is('.timeline-row')) {
                                    $(opts.wrapper).isotope('appended', items.parent('[class*="col-"]'));
                                }
                                $(opts.wrapper).on('layoutComplete', function() {
                                    $(opts.wrapper).addClass('items-loaded');
                                });
                                $.fn.matchHeight._afterUpdate = function(event, groups) {
                                    $(opts.wrapper).isotope('layout');
                                    $(opts.wrapper).addClass('items-loaded');
                                    setTimeout(function() {
                                        $(opts.wrapper).isotope('layout');
                                    }, 600);
                                };
                            }
                            if ('vc_js' in window) {
                                window.setTimeout(vc_waypoints, 500);
                                if ($(opts.wrapper).data('isotope')) {
                                    window.setTimeout(function() {
                                        $(opts.wrapper).isotope('layout');
                                    }, 600);
                                }
                            }
                        });
                        $(window).trigger('resize');
                    }
                });
            });
        }
    };
    $.fn.RellaAjaxLoadMore = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new AjaxLoadMore(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $(document).RellaAjaxLoadMore();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    if ($('[data-enable-fullpage]').length) {
        return;
    }
    var instanceName = '__LocalScroll';
    var LocalScroll = function(el, options) {
        return this.init(el, options);
    };
    LocalScroll.defaults = {};
    LocalScroll.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, LocalScroll.defaults, options);
            return this;
        },
        build: function() {
            var el = $('.local-scroll'), isOnePageNav = $('.main-nav').find('.local-scroll').length || $('.custom-menu').find('a:not([href="#"]):not([href="#0"])').length ? true : false;
            if (isOnePageNav) {
                $('body').scrollspy({
                    target: '#main-header-nav'
                });
            }
            if (!el.length) {
                if ($('.custom-menu').find('a:not([href="#"]):not([href="#0"])').length) {
                    el = $('.custom-menu').find('a:not([href="#"]):not([href="#0"])').parent();
                } else {
                    return;
                }
            }
            el.on('click', 'a:not([href="#"]):not([href="#0"])', function(event) {
                var $this = $(this);
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 800, function() {
                            $('.navbar-toggle', '.module-nav-trigger .module-trigger').trigger('click');
                            $('.header-module').removeClass('is-active module-container-is-showing');
                        });
                    }
                }
            });
        }
    };
    $.fn.RellaLocalScroll = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new LocalScroll(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $(document).RellaLocalScroll();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    var instanceName = '__SortingOption';
    var SortingOption = function(el, options) {
        return this.init(el, options);
    };
    SortingOption.defaults = {};
    SortingOption.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, SortingOption.defaults, options);
            return this;
        },
        build: function() {
            var el = $('.sorting-option');
            if (!el.length) {
                return;
            }
            el.each(function() {
                var $this = $(this), checkbox = $this.find('input[type=checkbox]');
                checkbox.on('change', function() {
                    if (checkbox.prop('checked')) {
                        $this.addClass('checked');
                    } else {
                        $this.removeClass('checked');
                    }
                });
            });
        }
    };
    $.fn.RellaSortingOption = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new SortingOption(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $('.sorting-option').RellaSortingOption();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    var instanceName = '__elementInView';
    var elementInView = function(el, options) {
        return this.init(el, options);
    };
    elementInView.defaults = {};
    elementInView.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, elementInView.defaults, options);
            return this;
        },
        build: function() {
            var element = $(this.el);
            var inViewCallback = function(enteries, observer) {
                enteries.forEach(function(entery) {
                    if (entery.isIntersecting) {
                        element.addClass('is-in-view');
                    }
                });
            };
            var options = {
                threshold: .25
            };
            var observer = new IntersectionObserver(inViewCallback, options);
            var observerTarget = element.get(0);
            observer.observe(observerTarget);
        }
    };
    $.fn.RellaelementInView = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-inview-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new elementInView(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $('[data-element-inview]').RellaelementInView();
    });
}).apply(this, [ jQuery ]);

(function($) {
    'use strict';
    var instanceName = '__VideoBG';
    var VideoBG = function(el, options) {
        return this.init(el, options);
    };
    VideoBG.defaults = {};
    VideoBG.prototype = {
        init: function(el, options) {
            if (el.data(instanceName)) {
                return this;
            }
            this.el = el;
            this.setOptions(options).build();
            return this;
        },
        setOptions: function(options) {
            this.el.data(instanceName, this);
            this.options = $.extend(true, {}, VideoBG.defaults, options);
            return this;
        },
        build: function() {
            var el = $('.video-bg-player');
            if (!el.length) {
                return;
            }
            el.YTPlayer();
            el.on('YTPReady', function(e) {
                $(e.currentTarget).addClass('video-is-ready');
            });
        }
    };
    $.fn.RellaVideoBG = function(settings) {
        return this.map(function() {
            var el = $(this);
            if (el.data(instanceName)) {
                return el.data(instanceName);
            } else {
                var pluginOptions = el.data('plugin-options'), opts;
                if (pluginOptions) {
                    opts = $.extend(true, {}, settings, pluginOptions);
                }
                return new VideoBG(el, opts);
            }
        });
    };
    $(document).ready(function() {
        $('.video-bg-player').RellaVideoBG();
    });
}).apply(this, [ jQuery ]);

(function($) {
    var Themerella = {
        init: function() {
            'use strict';
            this.themeRTL();
            this.modules();
            this.rellaAnimations();
            this.backToTop();
            this.accordion();
            this.postShare();
            this.promoteBox();
        },
        debounce: function(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this, args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        },
        themeRTL: function() {
            var isRTL = $('html').attr('dir') == 'rtl', fullwidthRow = $('[data-vc-full-width="true"]');
            if (isRTL && fullwidthRow.length) {
                fullwidthRow.each(function() {
                    var $this = $(this), elMarginLeft = parseInt($this.css('margin-left'), 10), elMarginRight = parseInt($this.css('margin-right'), 10), fullwidthPlaceholder = $this.next('.vc_row-full-width');
                    $this.css({
                        left: '',
                        right: ''
                    });
                    $this.css({
                        left: '',
                        right: (fullwidthPlaceholder.offset().left - elMarginRight - 30) * -1
                    });
                });
            }
        },
        rellaAnimations: function() {
            var rellaAnimateElement = $(".rella_animate_when_almost_visible:not(.wpb_start_animation)");
            if (rellaAnimateElement.length) {
                rellaAnimateElement.each(function() {
                    var $this = $(this), animationDelay = parseInt($this.attr('data-animation-delay'), 10) / 1e3 || 0;
                    $this.css({
                        '-webkit-animation-delay': animationDelay + 's',
                        'animation-delay': animationDelay + 's'
                    });
                });
                rellaAnimateElement.waypoint(function() {
                    var $this = $(this);
                    $this.addClass('wpb_start_animation animated');
                }, {
                    offset: "85%"
                });
            }
        },
        modules: function() {
            'use strict';
            var content = $('#content, #footer'), moduleTrigger = content.find('.module-trigger');
            moduleTrigger.each(function() {
                var $this = $(this), parentModule = $this.parent(), moduleContainer = $this.siblings('.module-container');
                $this.off('mouseenter mouseleave');
                parentModule.off('mouseenter mouseleave', '.moduleTrigger');
                parentModule.on('mouseenter', '.module-trigger', function() {
                    moduleContainer.stop().slideDown(300);
                    $this.addClass('module-container-is-showing');
                    if ($this.find('input[type="search"]').length) {
                        setTimeout(function() {
                            $this.find('input[type="search"]').focus();
                        }, 250);
                    }
                    if (typeof progressively !== typeof undefined && $(this).find('.progressive__img').length) {
                        var enableProgressiveLoad = new RellaProgressiveAspectRatio();
                        enableProgressiveLoad.init();
                        progressively.render();
                    }
                }).on('mouseleave', function() {
                    moduleContainer.stop().slideUp(300);
                    $this.removeClass('module-container-is-showing');
                    if ($this.find('input[type="search"]').length) {
                        setTimeout(function() {
                            $this.find('input[type="search"]').blur();
                        }, 150);
                    }
                });
            });
        },
        objectFitPolyfill: function() {
            'use strict';
            if (typeof objectFitImages !== typeof undefined) {
                objectFitImages();
            }
        },
        preloader: function() {
            'use strict';
            var self = this, preloader = $('.page-loader'), preloaderStyle1 = $('.page-loader-style1'), preloaderStyle2 = $('.page-loader-style2'), preloaderStyle3 = $('.page-loader-style3'), preloaderInner = preloader.children('.page-loader-inner'), fadeDelay = parseInt(preloader.data('fade-delay'), 10) || 0, pageLoaded = false, waitForImageELements = $('.titlebar');
            var removePreloader = function() {
                $('body').addClass('page-loaded preloader-animation--started').removeClass('preloader-animation--not-started page-unloading');
                if (preloaderStyle1.length) {
                    var onPageLoad = Themerella.debounce(function() {
                        $(window).triggerHandler('resize');
                        $('body').removeClass('preloader-animation--not-started');
                    }, 250);
                    preloader.on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
                        onPageLoad();
                    });
                }
            };
            var pageLoad = function(event) {
                if ((typeof event !== typeof undefined || event !== null) && event.persisted) {
                    removePreloader();
                    return;
                }
                if (waitForImageELements.length) {
                    waitForImageELements.imagesLoaded({
                        background: true
                    }, function() {
                        setTimeout(function() {
                            removePreloader();
                        }, 80);
                    });
                } else {
                    removePreloader();
                }
            };
            var pageUnload = function() {
                $('body').addClass('preloader-animation--started page-unloading').removeClass('preloader-animation--not-started preloader-animation--done page-loaded');
            };
            if (preloaderStyle2.length || $('body').hasClass('preloader-style2')) {
                setTimeout(function() {
                    preloader.fadeOut();
                }, fadeDelay);
            }
            if (preloaderStyle3.length) {
                preloader.find('.curtain-back').on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
                    preloader.fadeOut();
                    $('body').addClass('preloader-animation--done').removeClass('preloader-animation--started');
                });
            }
            $(document).ready(pageLoad);
            if ('onpagehide' in window) {
                window.addEventListener('pageshow', pageLoad, false);
                window.addEventListener('pagehide', pageUnload, false);
            } else {
                window.addEventListener('unload', pageUnload, false);
            }
        },
        backToTop: function() {
            var el = $('.site-backtotop'), $window = $(window);
            if (!el.length) {
                return;
            }
            $window.on('scroll', function() {
                if ($window.scrollTop() >= $window.outerHeight() / 2) {
                    el.addClass('is-visible');
                } else {
                    el.removeClass('is-visible');
                }
            });
        },
        accordion: function() {
            if (location.hash !== '' && $(location.hash).length) {
                $(location.hash).addClass('in').closest('.accordion-item').addClass('active').siblings().removeClass('active').find('.in').removeClass('in').siblings('.accordion-toggle').find('a').attr('aria-expanded', false);
            }
            $('.accordion-collapse').on('show.bs.collapse', function(e) {
                var $this = $(this);
                $this.closest('.accordion-item').addClass('active');
                if (history.pushState) {
                    history.pushState(null, null, '#' + $(e.target).attr('id'));
                } else {
                    location.hash = '#' + $(e.target).attr('id');
                }
            });
            $('.accordion-collapse').on('shown.bs.collapse', function(e) {
                var $this = $(this), parent = $this.closest('.accordion-item'), $window = $(window), parentOffsetTop = parent.offset().top;
                if (parentOffsetTop <= $window.scrollTop() - 15) {
                    $('html, body').animate({
                        scrollTop: parentOffsetTop - 45
                    }, 1e3);
                }
            });
            $('.accordion-collapse').on('hide.bs.collapse', function() {
                $(this).closest('.accordion-item').removeClass('active');
            });
        },
        postShare: function() {
            var postShare = $('.post-share');
            postShare.on('click', 'a:not([href="#"])', function(e) {
                e.preventDefault();
                window.open(this.href, 'rellaShareWindow', 'width=600, height=650');
            });
        },
        promoteBox: function() {
            var element = $('.promote-box.collapse');
            if (!element.length) return;
            element.on('hidden.bs.collapse', function() {
                createCookie("ra_promote_box_stats", 'hidden', 1);
            });
            if (readCookie("ra_promote_box_stats") == 'hidden') {
                element.removeClass('in');
            }
            function createCookie(name, value, days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1e3);
                    var expires = "; expires=" + date.toGMTString();
                } else var expires = "";
                document.cookie = name + "=" + value + expires + "; path=/";
            }
            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            }
        }
    };
    $(document).ready(function() {
        Themerella.init();
        Themerella.preloader();
        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function() {
            var flickel = $('.flickity-enabled');
            if (flickel.length) {
                flickel.flickity('resize');
            }
        });
        +function($) {
            'use strict';
            function transitionEnd() {
                var el = document.createElement('bootstrap');
                var transEndEventNames = {
                    WebkitTransition: 'webkitTransitionEnd',
                    MozTransition: 'transitionend',
                    OTransition: 'oTransitionEnd otransitionend',
                    transition: 'transitionend'
                };
                for (var name in transEndEventNames) {
                    if (el.style[name] !== undefined) {
                        return {
                            end: transEndEventNames[name]
                        };
                    }
                }
                return false;
            }
            $.fn.emulateTransitionEnd = function(duration) {
                var called = false;
                var $el = this;
                $(this).one('bsTransitionEnd', function() {
                    called = true;
                });
                var callback = function() {
                    if (!called) $($el).trigger($.support.transition.end);
                };
                setTimeout(callback, duration);
                return this;
            };
            $(function() {
                $.support.transition = transitionEnd();
                if (!$.support.transition) return;
                $.event.special.bsTransitionEnd = {
                    bindType: $.support.transition.end,
                    delegateType: $.support.transition.end,
                    handle: function(e) {
                        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments);
                    }
                };
            });
        }(jQuery);
    });
    var wooButtonUpdate = function() {
        'use strict';
        var button = $('.woocommerce-cart .woocommerce input.update_cart');
        if (!button.length) {
            return;
        }
        if (button.attr('disabled') == 'disabled') {
            button.prop('disabled', false);
            var but = button.parent().find('button.input-generated-button');
            if (but.length) {
                but.prop('disabled', false);
            }
        }
    };
    $(document).on('spinnerAction', wooButtonUpdate);
    $(document).on('change input', 'div.woocommerce > form .cart_item :input', wooButtonUpdate);
    $(document).ajaxComplete(function() {
        if (typeof progressively !== typeof undefined && $('.progressive__img').length) {
            var enableProgressiveLoad = new RellaProgressiveAspectRatio();
            enableProgressiveLoad.init();
            progressively.render();
        }
    });
    $(window).on('resize', function() {
        Themerella.themeRTL();
    });
    $(window).on('load', function() {
        Themerella.objectFitPolyfill();
    });
})(jQuery);